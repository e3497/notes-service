# notes service

## build & run

### Run localy

```shell
npm install

npm run build && npm run start

curl 127.0.0.1:3000/health
```

### Run in Docker localy

```shell
docker build --tag example-notes-service:0.0.1 .

docker run -it --rm -p 3000:3000 example-notes-service:0.0.1
```

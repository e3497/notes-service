import Fastify, { FastifyInstance, RouteShorthandOptions } from 'fastify'
// import { Server, IncomingMessage, ServerResponse } from 'http'

const server: FastifyInstance = Fastify({
    logger: true
})

const opts: RouteShorthandOptions = {
    schema: {
        response: {
            200: {
                type: 'object',
                properties: {
                    status: {
                        type: 'string',
                        enum: ['UP', 'DOWN']
                    }
                }
            }
        }
    }
}

server.get('/health', opts, async (request, reply) => {
    return { status: 'UP' }
})

const start = async () => {
    try {
        await server.listen(3000, '0.0.0.0')

        // const address = server.server.address()
        // const port = typeof address === 'string' ? address : address?.port

    } catch (err) {
        server.log.error(err)
        process.exit(1)
    }
}
start()

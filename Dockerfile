FROM node:14.16.1-alpine

RUN apk --update add bash

RUN mkdir -p /var/www/app
WORKDIR /var/www/app

COPY package.json .
COPY npm-shrinkwrap.json .

RUN npm ci --only=production

COPY lib lib

CMD node lib/index.js
